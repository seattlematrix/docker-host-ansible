#!/bin/sh

# Install necessary packages
sudo apt update
sudo apt install -y ansible python-apt

# Backup base system information
dpkg --get-selections > dpkg--get-selections-pre_ansible-$(date +'%Y%m%d')
ansible localhost -m setup > setup_facts-pre_ansible-$(date +'%Y%m%d')
ansible localhost -m package_facts > package_facts-pre_ansible-$(date +'%Y%m%d')

# Run Ansible playbook
ansible-playbook sm-docker-host.yml --ask-become-pass
