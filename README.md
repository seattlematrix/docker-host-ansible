# Seattlematrix Docker Ansible

This will use ansible to setup Docker and docker-compose on an Ubuntu 20.04 LTS VM 

## Purpose
Seattlematrix Docker server

## Specs
- Targetting Ubuntu 20.04.1 LTS, Server edition
- Ansible setup and configuration
- Docker Compose services

# To Use
`./bootstrap.sh`

When it asks for `BECOME password`, it's requesting `root` via `sudo`, install Ansible dependencies, and run Ansible.


# TODO
- fix ssh authorized-keys
- fix adding to docker group

- fix UFW ports up, they are specific to Jitsi atm


